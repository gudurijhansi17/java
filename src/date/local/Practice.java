package date.local;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class Practice {
	public static void main(String[] args) {
		LocalDate today=LocalDate.now();
		System.out.println(today);
		
		LocalDate date=LocalDate.of(1997, 7, 12);
		System.out.println(date);
		
		DateTimeFormatter df=DateTimeFormatter.ofPattern("dd/MM/yyyy");
		System.out.println(df.format(date));
		
		LocalDate d=LocalDate.now();
		
		d=d.plusYears(50);
		System.out.println(d);
		
		d=d.minusYears(10);
		System.out.println(d);
		
		d=d.minusWeeks(65);
		System.out.println(d);
		
		LocalDate t=LocalDate.now();
		LocalDate da1=LocalDate.of(1999, 7, 12);
		System.out.println(ChronoUnit.DAYS.between(da1,t));
		
		System.out.println(ChronoUnit.MONTHS.between(da1, t));
		
		System.out.println(ChronoUnit.YEARS.between(da1, t));
		
		System.out.println(da1.compareTo(t));
		
		System.out.println("Hello");

		
		
	}
	

}
